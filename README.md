# Real-time Global Illumination using Irradiance Probes

Author: Šimon Sedláček

This repository contains results of my diploma thesis "Real-time Global Illumination using Irradiance Probes".
See DOC for diploma thesis draft.
See APP for runnable application binary with a testing scene.
