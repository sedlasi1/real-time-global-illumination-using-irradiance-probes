## Application demo

Demo of the application using the presented rendering method is available here.



Download WINDOWS_APP.zip for demo compiled for Windows x64 systems.

Download LINUX_APP.zip for demo compiled for Linux x64 systems.

Read README file inside of the package for instructions on how to start the application.

#### CONTROLS

* **L** - switch between contoling light or camera
* **W , A , S , D** - movement of camera / light
* **E , Q** - movement up/down of camera / light
* **F** - mouse movement control ON/OFF
* **T** - switch display between 3D view and lightmap textures
    * **1 , 2** - switch the view between main lightmap and rest of lightmaps

#### REQUIREMENTS
The application __requires GLSL of version 430__, if the version is not met, the application will terminate  
with the following lines:  
<pre>FAILED COMPILATION OF PROGRAM:  
        EXITING PROGRAM WITH CODE 260</pre> 
In that case, an update of your GPU driver is needed.
